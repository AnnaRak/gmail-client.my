<?php

namespace App\Http\Controllers;

use Dacastro4\LaravelGmail\Facade\LaravelGmail;
use Dacastro4\LaravelGmail\Services\Message\Mail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmailsController extends Controller
{
    public function redirectToProvider()
    {
        return LaravelGmail::redirect();
    }

    public function handleProviderCallback()
    {
        LaravelGmail::makeToken();
        return redirect('emails');
    }

    public function index()
    {
        $messages = LaravelGmail::message('inbox', 'sent')->preload()->all();
        return view('emails', ['messages' => $messages]);
    }

    public function loggingOut()
    {
        LaravelGmail::logout();
        return redirect('/');
    }

    public function sendEmail(): \Illuminate\Http\RedirectResponse
    {
        $email = $_POST['email'];
        $subject = $_POST['subject-text'];
        $message = $_POST['message-text'];

        $mail = new Mail();
        $mail->to($email)->from('someting@foo.com')->subject($subject)->message($message)->send();
        return back();
    }

    public function getEmailContent(Request $request): JsonResponse
    {
        $emailId = $request->get('emailId');
        $email = LaravelGmail::message()->preload()->get($emailId);

        return response()->json(['content' => $email->getHtmlBody()]);
    }

}
