<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <!-- Scripts -->
    <script src="{{ asset('js/gmail.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap"
          rel="stylesheet">
    <!-- Styles -->

    <title>Laravel</title>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button"></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            </div>
            <div class="container">
                <em style="color: #c51f1a">{{ LaravelGmail::user() }}</em>
                @if(LaravelGmail::check())
                    <div class="col-auto">
                        <a href="{{ url('oauth/gmail/logout') }}">
                            <button type="submit" class="btn btn-primary mb-3">Logout</button>
                        </a>
                        <a href="{{ url('emails') }}">
                            <button type="submit" class="btn btn-outline-primary mb-3">Show Emails</button>
                        </a>
                        <button type="submit" id="createBtn" class="btn btn-outline-primary mb-3" data-toggle="modal"
                                data-target="#createModal">Create a new email
                        </button>
                    </div>
                @else
                    <div class="col-auto">
                        <a href="{{ url('oauth/gmail') }}">
                            <button type="submit" class="btn btn-danger mb-3">Please login with <i
                                    class="fab fa-google-plus-square"></i></button>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </nav>
</div>
</body>
</html>
