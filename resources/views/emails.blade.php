@include('welcome')
<body>
<div class="container">
    <table class="table">
        @foreach($messages as $message)
            <thead class="thead-dark" style="background-color:#a1cbef">
            <tr>
                <th scope="col">Email title</th>
                <th scope="col">From</th>
                <th scope="col">Date</th>
                <th scope="col">Label</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $message->getSubject() }}
                    <button type="button" class="js-view btn btn-outline-dark" aria-label="View"
                            value="{{ $message->getId() }}">View
                    </button>

                </td>
                <td>{{ $message->getFromName() }}</td>
                <td>{{ $message->getDate() }}</td>
                @foreach($message->getLabels() as $label)
                    @if ($label == 'INBOX')
                        <td>{{$label}}</td>
                    @elseif ($label == 'SENT')
                        <td>{{$label}}</td>
                        @break
                    @endif
                @endforeach
            </tr>
            </tbody>
        @endforeach
    </table>
</div>
{{--Create a New Email Pop-up--}}
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('email') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email" class="col-form-label">To:</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                               id="email" required autocomplete="email">
                    </div>
                    <div class="form-group">
                        <label for="subject-text" class="col-form-label">Subject</label>
                        <input type="text" class="form-control" name="subject-text" id="subject-text">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea class="form-control" name="message-text" id="message-text"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{--Email Content Pop-up--}}
<div class="modal fade bd-example-modal-lg" id="emailContentModal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="emailContent"></label>
                <textarea id="emailContent"></textarea>
            </div>
        </div>
    </div>
</div>

<footer>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"
            integrity="sha512-XKa9Hemdy1Ui3KSGgJdgMyYlUg1gM+QhL6cnlyTe2qzMCYm4nAZ1PsVerQzTTXzonUR+dmswHqgJPuwCq1MaAg=="
            crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
</footer>
</body>

