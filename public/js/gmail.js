$(document).ready(() => {

    const getEmailContent = emailId => {
        $.ajax({
            url: `emails/${emailId}`,
            data: {
                emailId: emailId,
            },
            success: data => {
                CKEDITOR.instances['emailContent'].setData(data.content);
                $('#emailContentModal').modal('show');
            },
        });
    };


    $('.js-view').click(e => {
        getEmailContent(e.target.value);
    });


    /*
    * CKEDITOR
    */
    CKEDITOR.replace('emailContent');

})
