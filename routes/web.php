<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/emails');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/oauth/gmail', 'EmailsController@redirectToProvider')->name('oauth-gmail');
Route::get('/oauth/gmail/callback', 'EmailsController@handleProviderCallback');

Route::get('/emails', 'EmailsController@index')->name('emails');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/oauth/gmail/logout', 'EmailsController@loggingOut')->name('welcome');

Route::post('/email', 'EmailsController@sendEmail')->name('email');
Route::get('/emails/{email}', 'EmailsController@getEmailContent')->name('content');
